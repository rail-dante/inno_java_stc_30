package ru.inno.repositories;

import java.util.List;
import java.util.Optional;

/**
 * 17.12.2020
 * 40. JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CrudRepository<T, ID> {
    void save(T entity);
    void update(T entity);
    void delete(ID id);

    Optional<T> findById(ID id);
    // limit -> size, offset -> page * size
    List<T> findAll(Integer page, Integer size);
}
