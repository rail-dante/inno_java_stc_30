package ru.inno.repositories;

import ru.inno.models.Account;
import ru.inno.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * 17.12.2020
 * 40. JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AccountsRepositoryJdbcImpl implements AccountsRepository {

    //language=SQL
    private static final String SQL_FIND_ALL = "select paginated_users.*, c.*, paginated_users.id as a_id, c.id as car_id\n" +
            "from (select * from account order by id limit ? offset ?) paginated_users\n" +
            "         left join car c on paginated_users.id = c.driver_id\n" +
            "order by paginated_users.id;";
    //language=SQL
    private static final String SQL_FIND_ALL_BY_AGE = "select * from account where age = ?";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select c.id as car_id, * from account a " +
            "left join car c on a.id = c.driver_id where a.id = ?";
    //language=SQL
    private static final String SQL_INSERT = "insert into account (first_name, last_name, age, experience) values (?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_UPDATE = "update account set " +
            "first_name = ?, " +
            "last_name = ?, " +
            "age = ?, " +
            "experience = ? where id = ?";

    private DataSource dataSource;

    public AccountsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Car> carRowMapper = row ->
            Car.builder()
                    .id((long) row.getInt("car_id"))
                    .color(row.getString("color"))
                    .number(row.getString("number"))
                    .build();

    private RowMapper<Account> accountRowMapper = row ->
            Account.builder()
                    .id((long) row.getInt("a_id"))
                    .age(row.getInt("age"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .experience(row.getInt("experience"))
                    .cars(new ArrayList<>()) // положили ему пустой список
                    .build();

    private RowMapper<Account> accountWithCarsRowMapper = row -> {
        // создали пользователя
        Account account = accountRowMapper.mapRow(row);

//        // проверили, есть ли хотя бы одна машина у этого пользователя
//        // car_id вернулся нам из базы
//        if (row.getObject("car_id", Long.class) != null) {
//            // обработали первую машину
//            Car car = carInAccountRowMapper.mapRow(row);
//            // положили ее к этому пользователю
//            account.getCars().add(car);
//            car.setDriver(account);
//        }
//        // обработали все остальные машины
//        while (row.next()) {
//            Car car = carInAccountRowMapper.mapRow(row);
//            account.getCars().add(car);
//        }
        // задача - обработать все машины и перенести их в список для текущего пользователя
        do {
            // смотрим, есть ли на текущей строке машина
            if (row.getObject("car_id", Integer.class) != null) {
                // обработали первую машину
                Car car = carRowMapper.mapRow(row);
                // положили ее к этому пользователю
                account.getCars().add(car);
                car.setDriver(account);
            }
        } while (row.next()); // переходим к след строке если есть

        return account;
    };

    @Override
    public List<Account> findAllByAge(Integer age) {
        List<Account> accounts = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_BY_AGE)) {

            statement.setInt(1, age);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Account account = accountWithCarsRowMapper.mapRow(result);
                accounts.add(account);
            }
            result.close();
            return accounts;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Account entity) {
        try (Connection connection = dataSource.getConnection();
             // при создании объекта, который может посылать запросы в бд, указываю Statement.RETURN_GENERATED_KEYS
             // -> это значит, что запрос должен вернуть сгенерированные ключи из бд обратно в Java
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setInt(3, entity.getAge());
            statement.setInt(4, entity.getExperience());
            // выполнили запрос, и получили количество строк, которое мы вставили, оно не должно быть нулевым
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            // generatedIds - множество строк, которое содержит сгенерированные ключи после выполнения запроса
            ResultSet generatedIds = statement.getGeneratedKeys();
            // next() возвращает true, если в generatedIds что-то есть, если там ничего нет - значит что-то пошло не такх
            if (generatedIds.next()) {
                // из generatedIds получаем сгенерированный ключ с названием id
                int generatedId = generatedIds.getInt("id");
                // для сохраняемой сущности проставляем id, который получили для этой сущности из базы
                entity.setId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Account entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setInt(3, entity.getAge());
            statement.setInt(4, entity.getExperience());
            statement.setInt(5, entity.getId().intValue());
            // выполнили запрос, и получили количество строк, которое мы вставили, оно не должно быть нулевым
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(Long id) {
        // TODO: реализовать
    }

    @Override
    public Optional<Account> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, id.intValue());
            ResultSet result = statement.executeQuery();

            // есть ли пользователь
            if (result.next()) {
                return Optional.of(accountWithCarsRowMapper.mapRow(result));
            }

            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

//    @Override
//    public List<Account> findAll() {
//        List<Account> accounts = new ArrayList<>();
//
//        Connection connection = null;
//        Statement statement = null;
//        ResultSet result = null;
//        try {
//            connection = dataSource.getConnection();
//            statement = connection.createStatement();
//            result = statement.executeQuery(SQL_FIND_ALL);
//
//            while (result.next()) {
//                Account account = Account.builder()
//                        .id((long) result.getInt("id"))
//                        .age(result.getInt("age"))
//                        .firstName(result.getString("first_name"))
//                        .lastName(result.getString("last_name"))
//                        .experience(result.getInt("experience"))
//                        .build();
//                accounts.add(account);
//            }
//
//            return accounts;
//        } catch (SQLException e) {
//            throw new IllegalStateException(e);
//        } finally {
//            if (result != null) {
//                try {
//                    result.close();
//                } catch (SQLException throwables) {
//                    // ignore
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (SQLException throwables) {
//                    // ignore
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (SQLException throwables) {
//                    // ignore
//                }
//            }
//        }
//    }

    @Override
    public List<Account> findAll(Integer page, Integer size) {
        List<Account> accounts = new ArrayList<>();
        Map<Long, Account> processedAccounts = new HashMap<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            statement.setInt(1, size);
            statement.setInt(2, page * size);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                // id текущего пользователя
                Long currentUserId = result.getLong("a_id");

                // если мы еще не обработали текущего пользователя
                if (!processedAccounts.containsKey(currentUserId)) {
                    // обрабатываем
                    Account account = accountRowMapper.mapRow(result);
                    // добавляем в список
                    accounts.add(account);
                    // добавляем в список обработанных
                    processedAccounts.put(currentUserId, account);
                }
                // сюда попадаем когда пользователь уже гарантированно обработан
                // проверяем, есть ли вообще у пользователя машины
                if (result.getObject("car_id") != null) {
                    // получили id водителя
                    Long currentDriverId = result.getLong("driver_id");
                    // обработали машину
                    Car car = carRowMapper.mapRow(result);
                    // положим ее текущему пользователю
                    processedAccounts.get(currentDriverId).getCars().add(car);
                    car.setDriver(processedAccounts.get(currentDriverId));
                }
            }
            result.close();
            return accounts;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
