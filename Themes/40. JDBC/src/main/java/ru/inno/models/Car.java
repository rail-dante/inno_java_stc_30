package ru.inno.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * 21.12.2020
 * 40. JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@Builder
@ToString(exclude = "driver")
public class Car {
    private Long id;
    private String number;
    private String color;
    private Account driver;
}
