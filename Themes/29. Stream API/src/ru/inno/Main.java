package ru.inno;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// получить id и имена всех пользователей, у которых возраст >= 25 в упорядоченном по убыванию возраста
public class Main {

    public static void main(String[] args) {
	    User user0 = new User(1L, "User1", "Use1", 44);
	    User user1 = new User(2L, "User2", "User2", 30);
	    User user2 = new User(3L, "User3", "User3", 25);
	    User user3 = new User(4L, "User4", "User4", 48);
	    User user4 = new User(5L, "User5", "User5", 10);
	    User user5 = new User(6L, "User6", "User6", 15);
	    User user6 = new User(7L, "User7", "User7", 35);
	    User user7 = new User(8L, "User8", "User8", 44);
	    User user8 = new User(9L, "User9", "User9", 21);
	    User user9 = new User(10L, "User10", "User10", 40);

        List<User> users = new ArrayList<>();

        users.add(user0);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        users.add(user7);
        users.add(user8);
        users.add(user9);

        List<User> usersAfter25 = new ArrayList<>();

        // выбрали пользователей, которые старше 25-лет
        for (User user : users) {
            if (user.getAge() >= 25) {
                usersAfter25.add(user);
            }
        }

        //выполняем сортировку по убыванию возраста

        // usersAfter25.sort(Collections.reverseOrder(Comparator.comparingInt(User::getAge)));
        Collections.sort(usersAfter25, Collections.reverseOrder((o1, o2) -> {
            return o1.getAge() - o2.getAge();
        }));

//        System.out.println(usersAfter25);

        List<UserForReport> userForReports = new ArrayList<>();
        for (User user : usersAfter25) {
            UserForReport userForReport = new UserForReport(user.getId(), user.getFirstName());
            userForReports.add(userForReport);
        }

        System.out.println(userForReports);
    }
}
