package ru.inno.comparing;

import ru.inno.User;

import java.util.*;

/**
 * 25.11.2020
 * 29. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        User first = new User(15L, "Марсель", "Сидиков", 26);
        User second = new User(10L, "Камиль", "Лутфулин", 25);
//        System.out.println(first.compareTo(second));

        String text1 = "aaaa";
        String text2 = "bb";
        String text3 = "cbb";
        String text4 = "ccbba";
        String text5 = "ababccc";
        String text6 = "b";
        String text7 = "ca";
//        System.out.println(text1.compareTo(text2));

        List<String> lines = Arrays.asList(text1, text2, text3, text4, text5, text6, text7);

        Comparator<String> stringByLengthComparator = (o1, o2) -> {
            return o1.length() - o2.length();
        };

        Collections.sort(lines, stringByLengthComparator);
        System.out.println(lines);

    }
}
