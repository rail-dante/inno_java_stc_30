package ru.inno.method_reference;

import ru.inno.User;

import java.util.Comparator;

/**
 * 25.11.2020
 * 29. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersByAgeComparator implements Comparator<User> {
    @Override
    public int compare(User o1, User o2) {
        return o1.getAge() - o2.getAge();
    }
}
