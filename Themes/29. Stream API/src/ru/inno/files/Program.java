package ru.inno.files;

import ru.inno.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * 25.11.2020
 * 29. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
        try {
            Files.lines(Paths.get("input.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new User(Long.parseLong(array[0]), array[1], array[2], Integer.parseInt(array[3])))
                    .filter(user -> user.getAge() >= 300)
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
