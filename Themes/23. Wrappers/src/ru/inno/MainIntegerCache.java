package ru.inno;

/**
 * 02.11.2020
 * 23. Wrappers
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainIntegerCache {
    public static void main(String[] args) {
        Integer i1 = 120;
        Integer i2 = 120;

        Integer i3 = 129;
        Integer i4 = 129;

        System.out.println(i1 == i2);
        System.out.println(i3 == i4);
    }
}
