package ru.inno;

/**
 * 02.11.2020
 * 23. Wrappers
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User {
    private String firstName;
    private String lastName;

    // private int age; // age = 0
    private Integer age; // age = 0, age = null
}
