package ru.inno;

/**
 * 02.11.2020
 * 23. Wrappers
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainBoxing {
    public static void main(String[] args) {
        // boxing - примитивный тип оборачиваем в обертку
        Integer i = new Integer(10);
        // autoboxing
        Integer i1 = 10;

        // unboxing - из обертки забираем значение примитивного типа
        int i3 = i1.intValue();
        // autounboxing
        int i4 = i1;

        Integer age = null;

        if (age != null) {
            int i5 = age;
        }
    }
}
