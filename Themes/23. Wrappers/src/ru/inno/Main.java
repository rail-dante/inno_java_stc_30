package ru.inno;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // оберточные типы
        Integer i = 5;
        Character c = 'A';
        Boolean b = true;
        Long l = 105L;
        Double d = 3.14;
        Byte by = 10;
        Float f = 10.5f;
        Short s = 10;

        // 1. Содержать дополнительные методы
        int anInt = Integer.parseInt("1234");
        double aDouble = Double.parseDouble("1234");
        // 2. Содержат дополнительные константы
        System.out.println(Integer.MAX_VALUE);
        // 3. Допускают значение null

        // 4. они нужны для использования в Generics
        List<Integer> integers = new ArrayList<>();
        integers.add(10);
        int v = integers.get(0);
    }
}
