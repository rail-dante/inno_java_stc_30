package ru.inno.sockets.clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * 23.12.2020
 * 42. Socket Clients Exmaple
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SocketClient {
    private Socket client;

    private PrintWriter toServer; // поток, в который мы пишем сообщения для сервера
    private BufferedReader fromServer; // поток, из которого приходят сообщения с сервера

    public SocketClient(String host, int port) {
        try {
            client = new Socket(host, port); // подключаемся к серверу
            toServer = new PrintWriter(client.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            // запускаем побочный поток, который будет читать сообщения от сервера
            new Thread(receiverMessagesTask).start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void sendMessage(String message) {
        toServer.println(message);
    }

    // задача для побочного потока, чтобы в асинхронном режиме читать сообщения с сервера
    private Runnable receiverMessagesTask = () -> {
        while (true) {
            try {
                String messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    System.out.println(messageFromServer);
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    };
}
