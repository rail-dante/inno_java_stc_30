package ru.inno.interview;

import java.io.IOException;

class Program {
public static void main(String[] args){
        try {
            System.out.println("Hello!");
            throw new IllegalArgumentException("Bye!");
        } catch (IllegalArgumentException e) {
            try {
                throw new IOException(e.getMessage());
            } catch (Exception e1) {
                System.out.println(e1.getMessage());
            }
        }
    }
}