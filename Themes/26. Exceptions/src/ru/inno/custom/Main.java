package ru.inno.custom;

import java.util.Scanner;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();
        if (password.length() < 7) {
            throw new IllegalPasswordLengthException("Слишком короткий пароль");
        }
    }
}
