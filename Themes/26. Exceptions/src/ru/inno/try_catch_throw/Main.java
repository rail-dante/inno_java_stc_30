package ru.inno.try_catch_throw;

import java.util.Scanner;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static int divPositives(int x, int y) {
        if (x < 0 || y < 0) {
            // throw - ключевое слово, которое выбрасывает исключение
            throw new IllegalArgumentException("Отрицательные аргументы!");
        }
        return x / y;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a;
        int b;

        a = scanner.nextInt();
        b = scanner.nextInt();

        try {
            // блок, в котором возможно возникновение исключения
            int c = divPositives(a, b);
        } catch (ArithmeticException e) {
            // что делать, если словили исключение ArithmeticException
            System.out.println("Пожалуйста, не вводите 0, потому что " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println("Какие-то проблемы с аргументами, потому что " + e.getMessage());
        } catch (RuntimeException e) {
            System.out.println("Ну там вообще все плохо - " + e.getMessage());
        }

        try {
            // блок, в котором возможно возникновение исключения
            int c = divPositives(a, b);
        } catch (ArithmeticException | IllegalArgumentException e) {
            System.out.println("Проблемы с аргументами " + e.getMessage());
        } catch (RuntimeException e) {
            System.out.println("Ну там вообще все плохо - " + e.getMessage());
        }
    }
}
