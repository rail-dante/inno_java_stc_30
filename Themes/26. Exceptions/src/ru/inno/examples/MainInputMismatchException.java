package ru.inno.examples;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainInputMismatchException {
    public static void main(String[] args) {
//        InputMismatchException
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

    }
}
