package ru.inno.examples;

import java.util.Scanner;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainNullPointerException {
    public static void main(String[] args) {
//        NullPointerException
        Scanner scanner = null;

        int a;
        int b;

        a = scanner.nextInt();
        b = scanner.nextInt();

        System.out.println(a / b);
    }
}
