package ru.inno.examples;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainOutOfMemoryError {
    public static void main(String[] args) {
//        OutOfMemoryError
        String array[] = new String[Integer.MAX_VALUE];
    }
}
