package ru.inno.examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainFileNotFound {
    public static void main(String[] args) throws Exception {
//        FileNotFoundException
        InputStream input = new FileInputStream("input.txt");
    }
}
