package ru.inno.checked;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * 12.11.2020
 * 26. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SimpleFileReader {
    private static int BYTES_COUNT = 100;

    // переменная, которая указываем на входной поток байтов
    private InputStream inputStream;

    public String readFromFile(String fileName) {
        // открыли файл
        fileOpen(fileName);
        // создаем массив байтов
        byte bytes[] = new byte[BYTES_COUNT];
        // читаю байты из файла в массив
        try {
            inputStream.read(bytes);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        // создаю строку на основе байтов
        return new String(bytes);
    }

    // throws - говорит о том, что метод может выбросить проверяемое исключение
    // без этой инструкции или try-catch программа не сработает
    private void fileOpen(String fileName) {
        try {
            this.inputStream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
