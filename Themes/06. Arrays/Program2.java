// массив как ссылочный тип данных
class Program2 {
	public static void main(String[] args) {
		int a[] = {1, 2, 3, 4, 5, 6};
		int b[];
		b = a;
		b[3] = 7;

		System.out.println(a[3]);
	}
}