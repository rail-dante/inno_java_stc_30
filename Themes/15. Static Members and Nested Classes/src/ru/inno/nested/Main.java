package ru.inno.nested;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Table table = new Table();

        table.put("Марсель", 26);
        table.put("Денис", 30);
        table.put("Илья", 28);

        Table table1 = new Table();
        table1.put("Неля", 18);
        table1.put("Катерина", 23);
        table1.put("Полина", 18);
        table1.put("Регина", 18);

//        System.out.println(table.get("Марсель")); // 26
//        System.out.println(table.get("Денис"));// 30
//        System.out.println(table.get("Илья")); // 28
//
//        System.out.println(table1.get("Неля"));
//        System.out.println(table1.get("Катерина"));
//        System.out.println(table1.get("Мария"));

//        TablePrinter tablePrinter = new TablePrinter(table, 15, 10);
//        tablePrinter.printTable("Имя", "Возраст");

        // вы создали объект TablePrinter, который ассоциирован с объектом table1
        Table.TablePrinter printer1 = table1.new TablePrinter(20, 20);
        printer1.printTable("Имя", "Возраст");

        System.out.println("---------------------");
        Table.TablePrinter printer = table.new TablePrinter(20, 20);
        printer.printTable("Имя", "Возраст");

//        Table.TableEntry tableEntry = new Table.TableEntry("Маленький Марсель", 11);
    }
}
