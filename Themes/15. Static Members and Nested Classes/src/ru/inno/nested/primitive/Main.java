package ru.inno.nested.primitive;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.NestedClass staticNestedClass = new OuterClass.NestedClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
    }
}
