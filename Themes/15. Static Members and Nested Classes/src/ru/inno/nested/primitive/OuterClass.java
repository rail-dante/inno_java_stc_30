package ru.inno.nested.primitive;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/*
OuterClass -> Table
InnerClass -> TablePrinter
NestedClass -> TableEntry
 */
public class OuterClass {

    private int outerClassField;
    private static int outerClassStaticField;

    // StaticNestedClass, InnerClass - вложенные классы (Nested)
    // StaticNestedClass - вложенный класс (или статический вложенный, nested, static nested)
    // InnerClass - внутренний

    public static class NestedClass {
        public int nestedClassField;

        public void method() {
            System.out.println(nestedClassField + " " + outerClassStaticField);
        }
    }

    public class InnerClass {
        public int innerClassField;

        public void method() {
            System.out.println(innerClassField + " " + outerClassField + " " + outerClassStaticField);
        }
    }

}
