package ru.inno.builder.primitive;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Box {
    private int a;
    private int b;

    private Box(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public static Box create(int a, int b) {
        return new Box(a, b);
    }
}
