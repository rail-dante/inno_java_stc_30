package ru.inno.singleton;

/**
 * 12.10.2020
 * 15. Static Members and Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
//        Logger logger = new Logger("MyLogger");

        Logger logger = Logger.newLogger();
        logger.info("Программа запущена!");
        logger.error("Какая-то ошибка :(");

        Logger anotherLogger = Logger.newLogger();

    }
}
