package ru.inno.reader_writer;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

/**
 * 16.11.2020
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class WriterExample {
    public static void main(String[] args) throws Exception {
        Writer writer = new FileWriter("output.txt");
        writer.write("Привет, как дела? Что нового?");
        writer.close();

    }
}
