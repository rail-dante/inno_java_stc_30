package ru.inno.reader_writer;

import java.io.*;
import java.util.Arrays;

/**
 * 16.11.2020
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ReaderExample {
    public static void main(String[] args) throws Exception {
//        InputStream input = new FileInputStream("input.txt");
//        Reader reader = new InputStreamReader(input);
//        Reader reader = new FileReader("input.txt");
        Reader reader = new StringReader("Hello");
        char c = (char) reader.read();
        System.out.println(c);
        char text[] = new char[16];
        reader.read(text);
        System.out.println(Arrays.toString(text));
    }
}
