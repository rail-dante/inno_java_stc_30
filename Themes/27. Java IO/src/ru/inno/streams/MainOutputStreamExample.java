package ru.inno.streams;

import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * 16.11.2020
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainOutputStreamExample {
    public static void main(String[] args) throws Exception {
        OutputStream outputStream = new FileOutputStream("output.txt", true);
        outputStream.write(72);
        outputStream.write(72 + 32);
        outputStream.close();
    }
}
