package example1;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class MilitaryTransport extends Transport {
    protected int bulletsCount;

    public MilitaryTransport(double fuelAmount, double fuelConsumption, int bulletsCount) {
        super(fuelAmount, fuelConsumption);
        this.bulletsCount = bulletsCount;
    }

    public void fire() {
        bulletsCount--;
    }

    public int getBulletsCount() {
        return bulletsCount;
    }
}
