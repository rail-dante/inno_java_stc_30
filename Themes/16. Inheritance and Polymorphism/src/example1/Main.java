package example1;

public class Main {

    public static void main(String[] args) {
	    Tank tank = new Tank(100, 2, 10, 0.5);
	    tank.go(5);
	    tank.fire();
	    tank.fire();
	    tank.fire();
        System.out.println(tank.getFuelAmount());
        System.out.println(tank.getBulletsCount());

        Airplane airplane = new Airplane(350, 4, 30);
        airplane.go(10);
		System.out.println(airplane.getFuelAmount());

//		example1.Transport transport = new example1.Transport(100, 10);
//		example1.MilitaryTransport militaryTransport = new example1.MilitaryTransport(10,5, 5);
		Transport transport = airplane;
		MilitaryTransport militaryTransport = tank;

		transport.go(100);
		militaryTransport.stop();

		transport = tank;
		transport.stop();

    }
}
