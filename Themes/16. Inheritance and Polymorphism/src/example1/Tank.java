package example1;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Tank extends MilitaryTransport {

    private double angle;

    public Tank(double fuelAmount, double fuelConsumption, int bulletsCount, double angle) {
        // вызов конструктора предка
        super(fuelAmount, fuelConsumption, bulletsCount);
        this.angle = angle;
    }

    @Override
    public void stop() {
        System.out.println("СТОП МАШИНА!");
    }
    public void round() {
        if (this.angle > -1) {
            this.angle -= 0.1;
        } else if (this.angle < 1) {
            this.angle += 0.1;
        }
    }
}
