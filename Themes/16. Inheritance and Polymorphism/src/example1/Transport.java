package example1;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class Transport {
    protected double fuelAmount;
    protected double fuelConsumption;

    public Transport(double fuelAmount, double fuelConsumption) {
        this.fuelAmount = fuelAmount;
        this.fuelConsumption = fuelConsumption;
    }

    public abstract void stop();

    public void go(int km) {
        this.fuelAmount -= (km * fuelConsumption) / 100;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }
}
