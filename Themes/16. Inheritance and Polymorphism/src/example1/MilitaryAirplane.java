package example1;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MilitaryAirplane extends Airplane {
    private int bulletsCount;

    public MilitaryAirplane(double fuelAmount, double fuelConsumption, double length, int bulletsCount) {
        super(fuelAmount, fuelConsumption, length);
        this.bulletsCount = bulletsCount;
    }

    public void fire() {
        this.bulletsCount--;
    }

    public int getBulletsCount() {
        return bulletsCount;
    }

    @Override
    public void stop() {
        System.out.println("Отступаем, приземляюсь!");
    }
}
