package example2;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceOutputPrefixImpl implements DeviceOutput, DeviceInfo {

    private String prefix;

    public DeviceOutputPrefixImpl(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void print(String message) {
        System.out.println(this.prefix + " " + message);
    }

    @Override
    public void printInfo() {
        System.out.println("Вывод с префиксом");
    }
}
