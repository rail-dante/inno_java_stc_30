package example2;

import java.time.LocalTime;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceOutputTimeImpl implements DeviceOutput, DeviceInfo {

    @Override
    public void print(String message) {
        System.out.println(LocalTime.now().toString() + " " + message);
    }

    @Override
    public void printInfo() {
        System.out.println("Вывод со временем");
    }
}
