package example2;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainDI {
    public static void main(String[] args) {

        DeviceOutputPrefixImpl d1 = new DeviceOutputPrefixImpl("MESSAGE: ");
        DeviceOutputTimeImpl d2 = new DeviceOutputTimeImpl();
        DeviceInputPrefixImpl d3 = new DeviceInputPrefixImpl("FROM CONSOLE: ");
        DeviceInputSuffixImpl d4 = new DeviceInputSuffixImpl(" -> from console");

        DeviceOutput deviceOutput1 = d1;
        DeviceOutput deviceOutput = d2;
        DeviceInput deviceInput1 = d3;
        DeviceInput deviceInput = d4;

        IOService ioService = new IOService(deviceInput, deviceOutput);

        ioService.print("Hello!");
        System.out.println(ioService.read());

        DeviceInfo deviceInfo[] = {d1, d2, d3, d4};
        for (int i = 0; i < deviceInfo.length; i++) {
            deviceInfo[i].printInfo();
        }
    }
}
