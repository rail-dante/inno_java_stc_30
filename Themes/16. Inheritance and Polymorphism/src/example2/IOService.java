package example2;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// паттерн Facade
public class IOService {
    private DeviceInput deviceInput;
    private DeviceOutput deviceOutput;

    public IOService(DeviceInput deviceInput, DeviceOutput deviceOutput) {
        this.deviceInput = deviceInput;
        this.deviceOutput = deviceOutput;
    }

    public void print(String message) {
        this.deviceOutput.print(message);
    }

    public String read() {
        return this.deviceInput.read();
    }
}
