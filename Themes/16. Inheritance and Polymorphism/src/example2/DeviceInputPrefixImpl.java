package example2;

import java.util.Scanner;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceInputPrefixImpl extends AbstractDeviceInputScannerImpl implements DeviceInput, DeviceInfo {

    private String prefix;

    public DeviceInputPrefixImpl(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public String read() {
        return prefix + " " + scanner.nextLine();
    }

    @Override
    public void printInfo() {
        System.out.println("Вывод с префиксом");
    }
}
