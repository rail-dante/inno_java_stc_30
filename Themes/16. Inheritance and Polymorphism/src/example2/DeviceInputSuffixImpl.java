package example2;

import java.util.Scanner;

/**
 * 15.10.2020
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceInputSuffixImpl extends AbstractDeviceInputScannerImpl implements DeviceInput, DeviceInfo {

    private String suffix;

    public DeviceInputSuffixImpl(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String read() {
        return scanner.nextLine() + " " + suffix;
    }

    @Override
    public void printInfo() {
        System.out.println("Ввод с суффиксом");
    }
}
