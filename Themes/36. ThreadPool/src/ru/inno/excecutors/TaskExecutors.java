package ru.inno.excecutors;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TaskExecutors {
    public static TaskExecutorService threadPerTask() {
        return new TaskExecutorThreadPerTaskImpl();
    }

    public static TaskExecutorService workerThread() {
        return new WorkerThreadImpl();
    }

    public static TaskExecutorService threadPool(int threadsCount) {
        return new ThreadPoolImpl(threadsCount);
    }
}
