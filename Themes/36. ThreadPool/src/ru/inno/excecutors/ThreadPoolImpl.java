package ru.inno.excecutors;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThreadPoolImpl implements TaskExecutorService {

    private final Deque<Runnable> tasks;

    private final WorkerThread workerThreads[];

    public ThreadPoolImpl(int threadsCount) {
        this.tasks = new LinkedList<>();
        this.workerThreads = new WorkerThread[threadsCount];
        for (int i = 0; i < workerThreads.length; i++) {
            this.workerThreads[i] = new WorkerThread("poolWorker-" + i);
            this.workerThreads[i].start();
        }
    }

    @Override
    public void submit(Runnable task) {
        // если кладем новую задачу в очередь, то мы не хотим, чтобы в этот момент ее меняли
        synchronized (tasks) {
            tasks.add(task);
            // оповестим поток, который был в ожидании о том, что есть новая задача
            tasks.notify();
        }
    }

    private class WorkerThread extends Thread {

        public WorkerThread(String threadName) {
            super(threadName);
        }
        @Override
        public void run() {
            Runnable task;
            while (true) {
                // заблокировать очередь задач, чтобы пока мы ее смотрим, никто больше не мог туда ничего положить
                synchronized (tasks) {
                    while (tasks.isEmpty()) {
                        // уходим в ожидание, пока в очередь ничего не положат
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    task = tasks.poll();
                }
                task.run();
            }
        }
    }
}
