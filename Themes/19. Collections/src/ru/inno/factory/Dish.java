package ru.inno.factory;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// PRODUCT
public interface Dish {
    void get();
}
