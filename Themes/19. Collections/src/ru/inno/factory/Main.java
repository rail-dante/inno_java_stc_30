package ru.inno.factory;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Chief chief = new MeatChief();
        Dish dish = chief.cook();

        dish.get();
    }
}
