package ru.inno;


public class MainArrayList {

    public static void main(String[] args) {
        List list = new ArrayList();

        for (int i = 0; i < 17; i++) {
            list.add(i);
        }

        list.removeFirst(8);
        System.out.println(list.get(8));
        System.out.println(list.indexOf(11));
        System.out.println(list.size());
        System.out.println(list.contains(15));

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        int i = 0;
    }
}
