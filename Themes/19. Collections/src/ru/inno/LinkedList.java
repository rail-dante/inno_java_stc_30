package ru.inno;

import org.w3c.dom.Node;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// список на основе узлов
// очень медленное получение элемента по индексу - O(N)
// быстрое добавление в конец - О(1)
// быстрое добавление в начало - О(1)
// долгое удаление - О(N)
// не требует расширения памяти
// размер не ограничен (только оперативной памятью, которую выделили для Java)
// memory - (VALUE + LINK) * N
public class LinkedList implements List {

    private class LinkedListIterator implements Iterator {

        @Override
        public int next() {
            return 0;
        }

        @Override
        public boolean hasNext() {
            return false;
        }
    }

    private Node first;
    private Node last;

    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return -1;


    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {

    }

    @Override
    public void insert(int element, int index) {

    }

//    // добавление в конец - O(N)
//    @Override
//    public void add(int element) {
//        Node newNode = new Node(element);
//        // если в списке ничего нет
//        if (this.first == null) {
//            // то новый элемент и есть первый элемент списка
//            this.first = newNode;
//        } else {
//            // если в списке уже есть элементы, нужно дойти до последнего
//            Node current = this.first;
//            // пока следующий после предыдущего не null
//            while (current.next != null) {
//                // идем дальше
//                current = current.next;
//            }
//            // сюда попали, когда дошли до последнего элемента
//            current.next = newNode;
//        }
//        count++;
//    }

    // Добавление в конец - О(1)
    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
            // новый узел теперь последний
            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(int element) {
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {

    }

    // реализация метода получения объекта
    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
