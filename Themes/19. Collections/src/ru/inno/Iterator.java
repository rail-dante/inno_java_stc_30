package ru.inno;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// PRODUCT
public interface Iterator {
    // возвращает следующий элемент
    int next();
    // проверяет, есть ли следующий элемент?
    boolean hasNext();
}
