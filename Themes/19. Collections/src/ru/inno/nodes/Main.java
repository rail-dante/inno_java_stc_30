package ru.inno.nodes;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // создали независимые узлы
        Node a = new Node(7);
        Node b = new Node(-3);
        Node c = new Node(6);
        Node d = new Node(5);

        // a -> b
        // b -> c
        // c -> d
        a.setNext(b);
        b.setNext(c);
        c.setNext(d);

        // создаем переменную, которая указывает на тот же объект что и a
        Node current = a;
        // пока эта переменная не null
        while (current != null) {
            // печатаем ее значение
            System.out.println(current.getValue());
            // теперь эта переменная указывает на объект, который следовал после этой переменной
            current = current.getNext();
        }
        int i = 0;
    }
}
