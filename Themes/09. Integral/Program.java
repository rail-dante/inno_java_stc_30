class Program {
	public static double f(double x) {
		return Math.sin(x);
	}

	public static double integralByRectangles(double a, double b, int n) {
		double h = (b - a) / n;

		double result = 0;

		for (double x = a; x <= b; x = x + h) {
			result = result + f(x) * h;
		}

		return result;
	}

	public static void printIntegralResultsForN(double a, double b, int ns[]) {
		for (int i = 0; i < ns.length; i++) {
			System.out.println("For N = " + ns[i] 
				+ ", result = " + integralByRectangles(a, b, ns[i]));
		}
	}

	public static void main(String[] args) {
		int ns[] = {10, 100, 1_000, 10_000, 100_000, 1000_000};

		printIntegralResultsForN(0, 10, ns);
	}
}