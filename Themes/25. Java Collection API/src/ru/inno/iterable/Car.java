package ru.inno.iterable;

import java.util.Iterator;

/**
 * 09.11.2020
 * 25. Java Collection API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Car implements Iterable<PartOfCar> {

    private Wheel wheel = new Wheel();
    private Driver driver = new Driver();
    private Engine engine = new Engine();

    private class CarIterator implements Iterator<PartOfCar> {

        private int indexOfPart = 0;

        @Override
        public boolean hasNext() {
            return indexOfPart < 3;
        }

        @Override
        public PartOfCar next() {
            PartOfCar result = null;
            if (indexOfPart == 0) {
                result =  wheel;
            } else if (indexOfPart == 1) {
                result = driver;
            } else if (indexOfPart == 2) {
                result = engine;
            }
            indexOfPart++;
            return result;
        }
    }

    @Override
    public Iterator<PartOfCar> iterator() {
        return new CarIterator();
    }
}
