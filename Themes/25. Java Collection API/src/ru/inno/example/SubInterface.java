package ru.inno.example;

/**
 * 09.11.2020
 * 25. Java Collection API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SubInterface extends SomeInterface {
    void hello();
    void print();
}
