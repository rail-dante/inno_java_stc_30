package ru.inno.strategy;

import java.util.Arrays;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainForStrategy {
    public static void main(String[] args) {
        SequenceImpl sequence = new SequenceImpl(new int[]{3, 2, 6, 7, 8, 100, -5, 10, 30, 48});
        sequence.setForLessElements(new SearchAlgorithmTrivialImpl(sequence.getSequence()));
        sequence.setForMoreElements(new SearchAlgorithmBinaryImpl(sequence.getSequence()));


        System.out.println(sequence.search(6));
        System.out.println(sequence.search(3));
        System.out.println(sequence.search(99));

        System.out.println(Arrays.toString(sequence.getSequence()));
    }
}
