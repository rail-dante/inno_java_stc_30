package ru.inno.strategy;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// последовательность чисел
public class SequenceImpl implements Sequence {
    // массив, который хранит последовательность
    private int sequence[];

    private static final int VALUE_FOR_SWITCH_SORT_ALGO = 5;

    private SearchAlgorithm forLessElements;
    private SearchAlgorithm forMoreElements;

    public void setForLessElements(SearchAlgorithm forLessElements) {
        this.forLessElements = forLessElements;
    }

    public void setForMoreElements(SearchAlgorithm forMoreElements) {
        this.forMoreElements = forMoreElements;
    }

    // конструктор последовательности, принимаем на вход исходный массив
    public SequenceImpl(int sequence[]) {
        // создаем копию исходного массива к нам
        this.sequence = new int[sequence.length];
        System.arraycopy(sequence, 0, this.sequence, 0, sequence.length);
//        for (int i = 0; i < sequence.length; i++) {
//            this.sequence[i] = sequence[i];
//        }
    }

    // возвращает true, если element есть в нашем массиве sequence
    @Override
    public boolean search(int element) {
        if (sequence.length <= VALUE_FOR_SWITCH_SORT_ALGO) {
            return forLessElements.search(element);
        } else {
            return forMoreElements.search(element);
        }
    }

    public int[] getSequence() {
        return sequence;
    }
}
