package ru.inno.strategy;

import java.util.Arrays;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Sequence sequence = new SequenceImpl(new int[]{3, 2, 6, 7, 8, 10});
        System.out.println(sequence.search(10));
        System.out.println(sequence.search(15));
        System.out.println(sequence.search(2));
    }
}
