package ru.inno.strategy;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SearchAlgorithmTrivialImpl implements SearchAlgorithm {

    private int sequence[];

    public SearchAlgorithmTrivialImpl(int[] sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean search(int element) {
        // проходим все элементы массива
        for (int i = 0; i < sequence.length; i++) {
            // если нашли совпадающий
            if (sequence[i] == element) {
                // говорим, что нашли элемент
                return true;
            }
        }
        // если ни разу не попали в условие, значит элемента нет
        return false;
    }
}
