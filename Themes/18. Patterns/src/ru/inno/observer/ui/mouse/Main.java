package ru.inno.observer.ui.mouse;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] argv) {
        Mouse mouse = new Mouse();

        mouse.onClick( (args, intensive)  -> {
            if (args.getClickedButton().equals(MouseButton.LEFT)) {
                System.out.println("была нажата левая кнопка!");
            } else if (args.getClickedButton().equals(MouseButton.RIGHT)) {
                System.out.println("была нажата правая кнопка!");
            }
            System.out.println(intensive);
        });

        mouse.clickLeftButton();
        mouse.clickRightButton();
    }
}
