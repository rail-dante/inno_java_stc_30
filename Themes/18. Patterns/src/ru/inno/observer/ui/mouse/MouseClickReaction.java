package ru.inno.observer.ui.mouse;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface MouseClickReaction {
    void handle(MouseClickArgs args, double intensive);
}
