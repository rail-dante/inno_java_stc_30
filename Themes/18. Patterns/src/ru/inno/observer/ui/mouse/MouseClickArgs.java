package ru.inno.observer.ui.mouse;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MouseClickArgs {
    private MouseButton clickedButton;

    public MouseClickArgs(MouseButton clickedButton) {
        this.clickedButton = clickedButton;
    }

    public MouseButton getClickedButton() {
        return clickedButton;
    }
}
