package ru.inno.observer.ui.mouse;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public enum MouseButton {
    LEFT(100),
    RIGHT(50),
    MIDDLE(0);

    private double intensive;

    MouseButton(double intensive) {
        this.intensive = intensive;
    }

    public double getIntensive() {
        return this.intensive;
    }




}
