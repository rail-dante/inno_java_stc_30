package ru.inno.observer.ui.enums;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public enum State {
    ACTIVE, BANNED, DELETED;
}
