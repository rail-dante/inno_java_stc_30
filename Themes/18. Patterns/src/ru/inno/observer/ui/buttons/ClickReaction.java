package ru.inno.observer.ui.buttons;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// Observer
public interface ClickReaction {
    void handle();
}
