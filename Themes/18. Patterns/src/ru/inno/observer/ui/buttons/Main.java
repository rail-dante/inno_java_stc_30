package ru.inno.observer.ui.buttons;

public class Main {

    public static void main(String[] args) {
        Button button = new OnOffButton();
        Button button1 = new OnOffButton();

//        ClickReaction clickReaction = new ClickReaction() {
//            @Override
//            public void handle() {
//                System.out.println("Кнопку нажали!");
//            }
//        };

        button.onClick(() -> System.out.println("Кнопку нажали!"));
        button.click();

        button1.onClick(() -> System.exit(-255));
        button1.click();
    }
}
