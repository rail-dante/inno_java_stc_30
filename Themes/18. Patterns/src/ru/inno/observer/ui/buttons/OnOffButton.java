package ru.inno.observer.ui.buttons;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class OnOffButton implements Button {

    private ClickReaction reaction;

    @Override
    public void click() {
        reaction.handle();
    }

    @Override
    public void onClick(ClickReaction clickReaction) {
        this.reaction = clickReaction;
    }
}
