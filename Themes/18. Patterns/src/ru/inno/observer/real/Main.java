package ru.inno.observer.real;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        TextProcessor textProcessor = new TextProcessorImpl();

        textProcessor.addObserver(c -> {
            if (Character.isDigit(c)) {
                System.out.println("это цифра!");
            }
        });

        textProcessor.addObserver(c -> {
            if (Character.isLetter(c)) {
                System.out.println("Это буква!");
            }
        });

        textProcessor.addObserver(c -> {
            System.out.println(c);
        });

        textProcessor.process("Hello, 1234!");
    }
}
