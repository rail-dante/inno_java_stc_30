package ru.inno.proxy.real;

/**
 * 22.10.2020
 * 18. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersServiceProxy extends UsersService {

    private BeforeUserOperation before;

    public UsersServiceProxy(BeforeUserOperation before) {
        this.before = before;
    }

    @Override
    public void signUp(String email, String password) {
        before.execute(email);
        super.signUp(email, password);
    }

    @Override
    public void signIn(String email, String password) {
        before.execute(email);
        super.signIn(email, password);
    }

    @Override
    public void resetPassword(String email) {
        before.execute(email);
        super.resetPassword(email);
    }
}
