-- создание таблицы
create table account
(
    id         serial primary key, -- первичный ключ, значение всегда должно быть уникальным
    first_name char(20),           -- строка длины 20
    last_name  char(20),           -- строка длины 20
    age        integer not null default 0
);

create table car
(
    id        serial primary key,
    number    char(6)
        constraint correct_number check ( length(number) = 6),
    color     char(10),
    driver_id integer,
    foreign key (driver_id) references account (id)
);

-- добавление данных в таблицу
insert into account(first_name, last_name, age)
values ('Марсель', 'Сидиков', 26);
insert into account(first_name, last_name, age)
values ('Андрей', 'Сачивко', 23);
insert into account(first_name)
values ('Камиль');
insert into account (first_name, last_name, age)
values ('Влад', 'Яковлев', 25);

insert into car(number, color)
values ('О001АА', 'White');
insert into car(number, color, driver_id)
values ('О002АА', 'Black', 2);
insert into car(number, color, driver_id)
values ('О003AA', 'Red', 3);
insert into car(number, color, driver_id)
values ('123456', 'Метро', 3);
insert into car(number, color, driver_id)
values ('О003АА', 'Green', 4);

-- не сработает, потому что нарушаем первичный ключ
-- insert into car (number, color, driver_id) values ('123457', 'White', 10);

-- добавление колонки в таблицу
alter table account
    add experience integer not null default 1;
-- добавление ограничения в таблицу
alter table account
    add constraint correct_experience check (experience < 30 and experience >= 1);
alter table car
    add constraint unique_number unique (number);
-- обновление данных в таблице
update account
set age = 25
where id = 3;
update account
set last_name = 'ФАМИЛИЯ НЕ УКАЗАНА'
where last_name isnull;

-- не сработает, нарушаем constraint
-- update account set experience = 32 where id = 3;

-- все водители
select *
from account;

-- только имена и возраст всех водителей в порядке убывания возраста
select first_name, age
from account
order by age desc;

-- получить все встречающиеся возраста водителей (удаление дубликатов)
select distinct age
from account;

-- получить всех водителей, у которых есть машины
select first_name, last_name
from account
where id in (select distinct driver_id from car where driver_id notnull);

-- получить id водителя и количество его машин
select driver_id, count(driver_id)
from car
where driver_id notnull
group by driver_id;

-- JOIN-ы

-- получить всех водителей и все их машины (не показал машину, у которой нет водителя)
select *
from account a
         left join car c on a.id = c.driver_id;

-- получить все машины и их водителей (не показал водителя у которого нет машины)
select *
from account a
         right join car c on a.id = c.driver_id;

-- получить всех водителей с машинами и машины с водителями (не показал водителя без машины и машину без водителя)
select *
from account a
         inner join car c on a.id = c.driver_id;

-- получить всех водителей с машинами и машины с водителями (показал все)
select *
from account a
         full join car c on a.id = c.driver_id;

-- получить имена всех водителей и количество их машин
select a.first_name as name, count(c.id) as cars_count
from account a
         left join car c on a.id = c.driver_id group by a.id;

-- удаление таблицы
drop table account;