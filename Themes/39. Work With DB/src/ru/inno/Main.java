package ru.inno;

import java.sql.*;

public class Main {

	private static final String PG_USER = "postgres";
	private static final String PG_PASSWORD = "qwerty007";
	private static final String PG_URL = "jdbc:postgresql://localhost:5432/stc_db_2020";

    public static void main(String[] args) {
    	Connection connection = null; // интерфейс - подключение к БД
		Statement statement = null; // интерфейс - запросы в БД
		ResultSet resultSet = null; // интерфейс - результат запроса

		try {
			Class.forName("org.postgresql.Driver");
			// DriverManager - находит нужный драйвер и создает подключение
			connection = DriverManager.getConnection(PG_URL, PG_USER, PG_PASSWORD);
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select first_name, age from account");

			while (resultSet.next()) {
				System.out.println(resultSet.getString("first_name") + " "
						+ resultSet.getInt("age"));
			}
		} catch (SQLException | ClassNotFoundException throwables) {
			throw new IllegalArgumentException(throwables);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException throwables) {
					// ignore
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException throwables) {
					// ignore
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException throwables) {
					// ignore
				}
			}
		}
    }
}
