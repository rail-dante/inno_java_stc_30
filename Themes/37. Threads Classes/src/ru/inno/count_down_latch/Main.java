package ru.inno.count_down_latch;

import java.io.*;
import java.net.URL;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

/**
 * 07.12.2020
 * 36. ThreadPool
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static void saveFile(String fileName) {
        try {
            URL url = new URL(fileName);
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            String newFileName = Thread.currentThread().getName() + "_" + UUID.randomUUID().toString() + ".png";
            FileOutputStream fos = new FileOutputStream("images\\" + newFileName);
            fos.write(response);
            fos.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void main(String[] args) throws Exception {
        // мы ждем 30 скачанных файлов
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        CountDownLatch countDownLatch = new CountDownLatch(30);
        // запускаем 30 потоков, ни один поток не будет работать, пока не будет запущено все 10 штук
        TaskExecutorService service = TaskExecutors.threadPool(10, countDownLatch);
        BufferedReader bufferedReader = new BufferedReader(new FileReader("links.txt"));
        scanner.nextLine();
        for (int i = 0; i < 100; i++) {
            final String finalFileName = bufferedReader.readLine();
            service.submit(() -> saveFile(finalFileName));
            System.out.println("Из main была направлена задача № " + i);
        }
        // main-будет ждать, пока не будет выполнено 30 задач (скачано 30 файлов)
        countDownLatch.await(); // ждет, пока i не будет равен 0
        System.out.println("Скачано 30 файлов");
        System.exit(0);

    }
}
