package ru.inno.cyclic_barrier;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Main {

    public static double saveFile(String fileName) {
        try {
            URL url = new URL(fileName);
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            String newFileName = Thread.currentThread().getName() + "_" + UUID.randomUUID().toString() + ".png";
            FileOutputStream fos = new FileOutputStream("images\\" + newFileName);
            int sizeOfFile = response.length;
            fos.write(response);
            fos.close();
            return sizeOfFile;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void main(String[] args) throws Exception {
        List<Double> filesSize = Collections.synchronizedList(new ArrayList<>());

        Scanner scanner = new Scanner(System.in);
        int parties = scanner.nextInt();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("links.txt"));
        CyclicBarrier cyclicBarrier = new CyclicBarrier(parties, () -> {
            double sum = filesSize.stream().mapToDouble(value -> value).sum();
            System.out.println("Скачано " + parties + " файлов, общий объем - " + sum);
        });

	    String fileName = bufferedReader.readLine();
	    int currentLineIndex = 0;
	    while (fileName != null) {
	        final String finalFileName = fileName;
	        new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + " начал скачивание");
	            // каждый поток - скачивает файл
                double size = saveFile(finalFileName);
                filesSize.add(size);
                System.out.println(Thread.currentThread().getName() + " завершил скачивание");
                try {
                    // уходим в ожидание других потоков, пока их не наберется parties-штук
                    System.out.println(Thread.currentThread().getName() + " ушел в ожидание");
                    cyclicBarrier.await();
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
                // сюда попадаем, когда набралось parties-ожидающих потоков
                System.out.println(Thread.currentThread().getName() + " вышел из ожидания");
            }, "Thread-" + currentLineIndex).start();
	        fileName = bufferedReader.readLine();
	        currentLineIndex++;
        }
    }
}
