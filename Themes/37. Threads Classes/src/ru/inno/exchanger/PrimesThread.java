package ru.inno.exchanger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * 11.12.2020
 * 37. Threads Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrimesThread implements Runnable {

    private int from;
    private int to;
    private int partSize;
    private final Exchanger<List<Integer>> exchanger;

    public PrimesThread(int from, int to, int partSize, Exchanger<List<Integer>> exchanger) {
        this.from = from;
        this.to = to;
        this.partSize = partSize;
        this.exchanger = exchanger;
    }

    private boolean isPrimeSum(int number) {

        int sum = 0;

        while (number != 0) {
            sum += number % 10;
            number = number / 10;
        }

        if (sum == 2 || sum == 3) {
            return true;
        }

        for (int i = 2; i * i <= sum; i++) {
            if (sum % i == 0) {
                return false;
            }
        }


        return true;
    }

    @Override
    public void run() {
        int currentIndex = 0;
        // данный кусок содержит список всех чисел, у которых сумма цифр - простое число
        List<Integer> currentPart = new ArrayList<>();
        for (int i = from; i < to; i++) {
            // если сумма цифр текущего числа - простое число
            if (isPrimeSum(i)) {
                // кладем в список
                currentPart.add(i);
            }

            if (currentIndex == partSize) {
                try {
                    // передали текущий кусок в другой поток
                    exchanger.exchange(currentPart);
                    // начали заново
                    currentIndex = 0;
                    currentPart = new ArrayList<>();
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
            currentIndex++;
        }
    }
}
