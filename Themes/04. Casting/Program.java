class Program {
	public static void main(String[] args) {
		int a = 9;
		int b = 2;
		double c = a / b; // 9 / 2 = 4, int -> double
		System.out.println(c);

		double x = 9.0;
		double y = 2.0;
		int z = (int)(x / y); // double -> int
		System.out.println(z);
	}
}