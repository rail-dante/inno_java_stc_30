package ru.inno.webdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.inno.webdemo.models.User;
import ru.inno.webdemo.repositories.UsersRepository;

import java.util.List;

/**
 * 06.12.2020
 * 35. Web Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;

    @GetMapping("/users")
    public List<User> getUsers(@RequestParam(value = "firstName", required = false) String firstName) {
        // firstName = ель
        // like %eль%
        return usersRepository.findAllByFirstNameIsLike("%" + firstName + "%");
    }
}
