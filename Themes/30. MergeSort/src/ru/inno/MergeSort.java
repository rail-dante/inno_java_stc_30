package ru.inno;

/**
 * 26.11.2020
 * 30. MergeSort
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MergeSort {

    private static int helpArray[] = new int[200];

    public static void doSort(int array[]) {
        int lower = 0;
        int higher = array.length - 1;

        sort(array, lower, higher);
    }

    private static void sort(int array[], int lower, int higher) {
        LogUtils.log(array, "sort", lower, higher);
        // условие окончания рекурсии
        if (higher <= lower) {
            return;
        }
        // вычисляем середину
        // lower = 15
        // higher = 47
        // 15 + (47 - 15) / 2 = 15 + 16 = 31
        int middle = lower + (higher - lower) / 2;
        LogUtils.indentUp();
        sort(array, lower, middle); // сортируем левую часть
        sort(array, middle + 1, higher); // сортируем правую часть
        merge(array, lower, middle, higher);
        LogUtils.indentDown();
    }

    // слияние двух частей массива array[lower ... middle] и array[middle + 1 ... higher]
    private static void merge(int array[], int lower, int middle, int higher) {
        // копируем сливаемый участок массива array[lower ... higher] в вспомогательный массив
        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            helpArray[currentIndex] = array[currentIndex];
        }

        int i = lower, j = middle + 1;

        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            // если мы просмотрели всю левую половину
            if (i > middle) {
                // закидываем сразу элемент справа, потому что слева все элементы кончились
                array[currentIndex] = helpArray[j];
                j++;
            }
            // если мы просмотрели всю правую половину
            else if (j > higher) {
                // закидываем сразу элементы слева, потому что справа все элементы закончились
                array[currentIndex] = helpArray[i];
                i++;
            }
            // выбор очередного элемента
            else if (helpArray[j] < helpArray[i]) {
                // если a[j] < a[i], то выбираем a[j]
                array[currentIndex] = helpArray[j];
                j++;
            } else {
                // если a[j] >= a[i], то выбираем a[i]
                array[currentIndex] = helpArray[i];
                i++;
            }
        }
        LogUtils.log(array, "merge", lower, higher);
    }
}
