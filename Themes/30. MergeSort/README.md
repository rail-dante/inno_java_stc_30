# Сортировка слиянием

## Слияние

```
[12 55 -3 6] [12 66 133 -10]
      |            |
      V            V
[-3 6 12 55] [-10 12 66 133]
 ^            ^

[-10]        

[-3 6 12 55] [-10 12 66 133]
 ^                ^

[-10 -3]

[-3 6 12 55] [-10 12 66 133]
    ^              ^

[-10 -3 6]

[-3 6 12 55] [-10 12 66 133]
       ^           ^

[-10 -3 6 12]

[-3 6 12 55] [-10 12 66 133]
          ^        ^

[-10 -3 6 12 12]

[-3 6 12 55] [-10 12 66 133]
          ^          ^

[-10 -3 6 12 12 55]

[-3 6 12 55] [-10 12 66 133]
                      ^

[-10 -3 6 12 12 55 66 133]
```

## Общий алгоритм сортировки слиянием

* Есть массив `A` - он не отсортирован

```
left = Левая часть(A)
right = Правая часть(A)

sort(left)
    left = Левая часть(Левая часть(A))
    right = Правая часть(Левая часть(A))

    sort(left)
    sort(right)
    merge(left + right)
sort(right)
    left = Левая часть(Правая часть(A))
    right = Правая часть(Правая часть(A))

    sort(left)
    sort(right)
    merge(left + right)
merge(left + right)
```

## Реализация

```
[-10, 11, 12, 45] [-4, 10, 25, 30] - helpArray
               ^            ^
               i            j

currentIndex
 | 
 V
[-10, 11, 12, 45] [-4, 10, 25, 30] - array

array[currentIndex] = helpArray[i]
i++; 
currentIndex++;

     currentIndex
      | 
      V
[-10, 11, 12, 45] [-4, 10, 25, 30] - array

array[currentIndex] = helpArray[j]
j++;
currentIndex++;

         currentIndex
           | 
           V
[-10, -4, 12, 45] [-4, 10, 25, 30] - array

array[currentIndex] = helpArray[j]
j++;
currentIndex++;

             currentIndex
               | 
               V
[-10, -4, 10, 45] [-4, 10, 25, 30] - array

array[currentIndex] = helpArray[i];
i++;
currentIndex++;

                   currentIndex
                   | 
                   V
[-10, -4, 10, 11] [-4, 10, 25, 30] - array
array[currentIndex] = helpArray[i]
i++;
curretIndex++;

                        currentIndex
                        | 
                        V
[-10, -4, 10, 11] [12, 10, 25, 30] - array
array[currentIndex] = helpArray[i]
i++;
curretIndex++;

...

[-10, -4, 10, 11] [12, 25, 30, 45] - array
```

* Что значит все элементы просмотрены?

```
[35, 39, 40, 45] [-4, 10, 25, 30] - helpArray
  ^                              ^
  i                              j

j > higher

[-4, 10, 25, 30 ... ] + 35, 39, 40, 45
```

## Сложность алгоритма

```
sort
    sort - сортировка левой половины С(N/2)
    sort - сортировка правой половины C(N/2)
    merge - слияние N

N = 2^n
2^n / 2 = 2^n-1
2^5 / 2 = 32 / 2 = 16 = 2^4 = 2^5-1

C(N) = C(N/2) + C(N/2) + N

C(2^n) = C(2^n-1) + C(2^n-1) + N

C(2^n) = 2 * C(2^n-1) + 2^n - все разделим на 2^n

C(2^n)/2^n = 2C(2^n-1)/ 2^n + 1 - сокращаем двойки в 2C(2^n-1)/ 2^n

C(2^n)/2^n = C(2^n-1) / 2^n-1 + 1, введем обозначение C(2^n)/2^n -> aN, C(2^n-1)/2^n-1 -> aN-1

aN = aN-1 + 1

C(2^n-1) / 2^n-1 = C(2^n-2) / 2^n-2 + 1

C(2^n)/2^n = C(2^n-2) / 2^n-2 + 1 + 1

если мы n-раз повторим это преобразование

C(2^n)/2^n = 1 + 1 + 1 ... + 1 + C(1)

C(1) = 0

C(2^n)/2^n = n

C(N)/N = n

N = 2^n, n = log(2)N

C(N)/N = log(2)N

C(N) = NlogN -> O(NlogN)
```