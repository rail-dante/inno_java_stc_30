package ru.inno.base;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        EggThread eggThread = new EggThread();
        HenThread henThread = new HenThread();
        Tirex tirex = new Tirex();

        eggThread.start();
        henThread.start();
        new Thread(tirex).start();
        // мы из main-потока вызвали join у egg-потока
        // следовательно, main будет ждать полного завершения egg
        eggThread.join();
        henThread.join();

        for (int i = 0; i < 1000000; i++) {
            if (i % 10000 == 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
            System.out.println(Thread.currentThread().getName() + " Human");
        }

        scanner.nextLine();
    }
}
