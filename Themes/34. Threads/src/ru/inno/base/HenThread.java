package ru.inno.base;

import java.util.Random;

/**
 * 03.12.2020
 * 34. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HenThread extends Thread {

    private Random random = new Random();

    @Override
    public void run() {
        // описываем вычисления потока
        for (int i = 0; i < 1000000; i++) {
            if (i % 10000 == 0) {
                try {
                    Thread.sleep(random.nextInt(2000));
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
            System.out.println(Thread.currentThread().getName() + " Hen");
        }
    }
}
