package ru.inno.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * 03.12.2020
 * 34. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();

        Random random = new Random();

        for (int i = 0; i < 10000000; i++) {
            numbers.add(random.nextInt(100000000));
        }
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        long before = System.currentTimeMillis();
        numbers.parallelStream().forEach(number -> {
            if (number == 2 || number == 3) {
                System.out.println(number);
            }

            for (int i = 2; i * i < number; i++) {
                if (number % i == 0) {
                    return;
                }
            }
            System.out.println(number);

        });
        long after = System.currentTimeMillis();
        System.out.println(after - before);
    }
}
