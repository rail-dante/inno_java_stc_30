package ru.inno.service;

/**
 * 03.12.2020
 * 34. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThreadService {
    public void submit(Runnable task) {
        Thread newThread = new Thread(task);
        newThread.start();
    }
}
