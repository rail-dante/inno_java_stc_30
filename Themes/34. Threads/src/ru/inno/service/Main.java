package ru.inno.service;

/**
 * 03.12.2020
 * 34. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ThreadService threadService = new ThreadService();
        threadService.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("Marsel!");
            }
        });

        threadService.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("Sidikov!");
            }
        });

        threadService.submit(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("26!");
            }
        });
    }
}
