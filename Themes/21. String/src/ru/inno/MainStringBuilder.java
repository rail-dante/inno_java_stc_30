package ru.inno;

/**
 * 29.10.2020
 * 21. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainStringBuilder {
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        builder.append("Hello");
        for (int i = 0; i < 100; i++) {
            builder.append(i);
        }

        builder.append("Hello").append("Marsel").append("Bye!");
        String result = builder.toString();
    }
}
