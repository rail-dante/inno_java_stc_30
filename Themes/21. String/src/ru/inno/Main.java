package ru.inno;

public class Main {

    /*
    public boolean equals(Object var1) {
        // сравнивает ссылки
        if (this == var1) {
            return true;
        } else {
            // если экземпляр строки
            if (var1 instanceof String) {
                // делаем явное преобразование
                String var2 = (String)var1;
                // берем длину нашей строки
                int var3 = this.value.length;
                // сравниваем с длиной другой строки если совпало
                if (var3 == var2.value.length) {
                    char[] var4 = this.value;
                    char[] var5 = var2.value;
                    // посимвольно сравниваем две строки
                    for(int var6 = 0; var3-- != 0; ++var6) {
                        if (var4[var6] != var5[var6]) {
                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        }
    }
     */

    public static void main(String[] args) {
        String s1 = "java";
        String s2 = "java";
        String s3 = new String("java");
        String s4 = new String("java");
        System.out.println(s1.equals(s2));
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s1.equals(s3));
        System.out.println(s3 == s4); // false
        System.out.println(s3.equals(s4)); //true
    }
}
