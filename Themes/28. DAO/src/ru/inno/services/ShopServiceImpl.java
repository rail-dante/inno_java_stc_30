package ru.inno.services;

import ru.inno.dao.ProductsDao;
import ru.inno.dao.UsersDao;
import ru.inno.models.Product;
import ru.inno.models.User;

import java.util.Optional;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// класс бизнес-логики, реализованный по паттерну Фасад
public class ShopServiceImpl implements ShopService {

    private UsersDao usersDao;
    private ProductsDao productsDao;

    public ShopServiceImpl(UsersDao usersDao, ProductsDao productsDao) {
        this.usersDao = usersDao;
        this.productsDao = productsDao;
    }

    @Override
    public void buy(Long userId, Long productId) {
        // бизнес-процесс
        Optional<User> userOptional = usersDao.findById(userId);
        Optional<Product> productOptional = productsDao.findById(productId);

        if (userOptional.isPresent() && productOptional.isPresent()) {
            User user = userOptional.get();
            Product product = productOptional.get();

            user.setCash(user.getCash() - product.getPrice());
            product.setCount(product.getCount() - 1);

            usersDao.update(user);
            productsDao.update(product);
        }
    }
}
