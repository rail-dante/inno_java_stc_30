package ru.inno.utils;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IdGeneratorInMemoryImpl implements IdGenerator {
    private Long lastGeneratedId;

    public IdGeneratorInMemoryImpl() {
        this.lastGeneratedId = -1L;
    }

    @Override
    public Long nextId() {
        return ++this.lastGeneratedId;
    }
}
