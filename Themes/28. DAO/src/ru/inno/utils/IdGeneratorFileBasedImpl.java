package ru.inno.utils;

import java.io.*;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IdGeneratorFileBasedImpl implements IdGenerator {
    private String fileName;

    public IdGeneratorFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Long nextId() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String lastGeneratedIdAsString = bufferedReader.readLine();
            long newId = Long.parseLong(lastGeneratedIdAsString);
            newId++;
            bufferedReader.close();
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(Long.toString(newId));
            writer.close();
            return newId;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
