package ru.inno.examples.optional;

import ru.inno.models.User;

import java.util.Optional;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        FakeRepository fakeRepository = new FakeRepositoryImpl();
        // а что, если пользователь не найден?
        // 1. Вернется null
        // 2. Будет Exception
        // 3. Будет возвращен какой-либо дефолтный объект

        // User user = fakeRepository.findById(1L);

        // Optional - позволяет понять, что возвращает метод, избавиться от проверки на null
        Optional<User> userOptional = fakeRepository.findById(10L);

        if (userOptional.isPresent()) {
            System.out.println("Пользователь найден!");
            User user = userOptional.get();
            System.out.println(user.getFirstName() + " " + user.getLastName());
        } else {
            System.out.println("Не найден!");
        }

        // Optional - добавляет синтаксический сахар
        User user = userOptional.orElse(new User("DEFAULT", "DEFAULT"));
        System.out.println(user.getFirstName());
    }
}
