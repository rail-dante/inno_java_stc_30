package ru.inno.examples;

import ru.inno.dao.UsersDao;
import ru.inno.dao.UsersDaoFileBasedImpl;
import ru.inno.models.User;
import ru.inno.utils.IdGeneratorFileBasedImpl;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // задача - сохранять информацию о каждом пользователе
        // каждому пользователю присваивать случайное число, которое означает
        // номер его билета
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        // создал объект DAO, мне не важно, как хранятся пользователи
        // мне важно иметь возможность их сохранить
        UsersDao userDao =
                new UsersDaoFileBasedImpl("users.txt",
                        new IdGeneratorFileBasedImpl("users_sequence.txt"));


        while (true) {
            String firstName = scanner.nextLine();
            String lastName = scanner.nextLine();

            User user = new User(firstName, lastName);
            // я сохраняю пользователя, независимо от того, где, куда и как
            userDao.save(user);

            System.out.println("Повторить ввод? Y/N");
            String command = scanner.nextLine();
            if (command.equals("N")) {
                return;
            }
        }
    }
}
