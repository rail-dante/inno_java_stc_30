package ru.inno.dao;

import ru.inno.models.User;

import java.util.List;
import java.util.Optional;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// CREATE, READ, UPDATE, DELETE
public interface CrudDao<T> {
    Optional<T> findById(Long id);
    void save(T entity);
    void delete(T entity);
    List<T> findAll();
    void update(T entity);
}
