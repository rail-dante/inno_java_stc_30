package ru.inno.dao;

import ru.inno.models.Product;
import ru.inno.models.User;

/**
 * 19.11.2020
 * 28. DAO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ProductsDao extends CrudDao<Product> {
    User findByName(String name);
}
