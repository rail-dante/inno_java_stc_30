package ru.inno;

/**
 * 13.12.2020
 * 38. Maven and JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Parser parser = new ParserImpl();
        int value = parser.parse("12345");
        System.out.println(value + 1);
    }
}
