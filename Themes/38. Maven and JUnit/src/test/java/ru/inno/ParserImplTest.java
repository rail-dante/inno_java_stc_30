package ru.inno;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 13.12.2020
 * 38. Maven and JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
class ParserImplTest {

    private ParserImpl parser;

    @BeforeEach
    public void setUp() {
        parser = new ParserImpl();
    }

    @Test
    public void testOnCorrectNumber() {
        String forTest = "12345";
        int expected = 12345;

        int actual = parser.parse(forTest);

        assertEquals(expected, actual);
    }

    @Test
    public void testOnCorrectPositiveNumber() {
        String forTest = "+12345";
        int expected = 12345;

        int actual = parser.parse(forTest);

        assertEquals(expected, actual);
    }

    @Test
    public void testOnCorrectNegativeNumber() {
        String forTest = "-12345";
        int expected = -12345;

        int actual = parser.parse(forTest);

        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @ValueSource(strings = {"-12345asd123", "", "aba"})
    public void testOnIncorrectNumbers(String forTest) {
        assertThrows(IllegalArgumentException.class, () -> {
            parser.parse(forTest);
        });
    }

//    @Test
//    public void testOnIncorrectNumber() {
//        String forTest = "-12345asd123";
//
//        assertThrows(IllegalArgumentException.class, () -> {
//            parser.parse(forTest);
//        });
//    }
//
//    @Test
//    public void testOnEmptyString() {
//        String forTest = "";
//
//        assertThrows(IllegalArgumentException.class, () -> {
//            parser.parse(forTest);
//        });
//    }
//

    @Test
    public void testOnNullString() {
        String forTest = null;

        assertThrows(IllegalArgumentException.class, () -> {
            parser.parse(forTest);
        });
    }


}