class Program2 {
	public static void from1ToN(int n) {
		System.out.println("--> n(" + n + ")");
		if (n == 1) {
			System.out.println(n);
		} else {
			from1ToN(n - 1);
			System.out.println(n);
			System.out.println("<-- n(" + n + ")");
		}
	}


	public static String isTwoPow(int a) {
		System.out.println("--> isTwoPow(" + a + ")");
		if (a == -1 || a == 1) {
			System.out.println("<-- isTwoPow(" + a + ") with YES");
			return "YES";
		}
		else if (a % 2 == 0) {
			String result = isTwoPow(a / 2);
			System.out.println("<-- isTwoPow(" + a + ") with " + result);
			return result;
		} else {
			System.out.println("<-- isTwoPow(" + a + ") with NO");
			return "NO";
		}
	}

	public static int digitsSum(int n) {
		if (n < 10) {
			return n; 
		} else {
			return n % 10 + digitsSum(n / 10);
		}
	}

	public static int fib(int n) {
		System.out.println("--> fib(" + n + ")");
		if (n == 1 || n == 2) {
			System.out.println("<-- fib(" + n + ") = 1");
			return 1;
		}
		int result =  fib(n - 1) + fib(n - 2);
		System.out.println("<-- fib(" + n + ") = " + result);
		return result;
	}

	public static void main(String[] args) {
		// from1ToN(10);
		int x = 2_147_483_647;
		x = x + 1;
		// System.out.println(isTwoPow(x));
		// System.out.println(digitsSum(123456789));
		System.out.println(fib(10));
	}
}
