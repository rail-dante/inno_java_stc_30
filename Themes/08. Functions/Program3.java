class Program3 {

	// number -> > 2
	public static boolean isPrime(int number) {
		if (number == 2 || number == 3) {
			return true;
		}
		// i <= sqrt(number)
		// i * i <= number
		for (int i = 2; i * i <= number; i++) {
			if (number % i == 0) {
				return false;
			}
		}

		return true;
	}

	public static int sum(int a, int b) {
		return a + b;
	}

	public static double sum(double a, double b) {
		return a + b;
	}

	// public static int sum(int a, int b, int c) {
	// 	return a + b + c;
	// }

	// public static double sum(int a, int b) {
	// 	return a + b;
	// }

	public static void main(String[] args) {
		// double result1 = sum(5, 6);
		sum(5, 6);
		// int result2 = sum(5, 6, 7);
		// int result3 = sum(1, 2, 3, 4);
		System.out.println(isPrime(121)); // false
		System.out.println(isPrime(69)); // false
		System.out.println(isPrime(15)); // false
		System.out.println(isPrime(113)); // true
	}
}