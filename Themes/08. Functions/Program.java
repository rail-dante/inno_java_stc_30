class Program {
	
	public static void main(String[] args) {
		// int value = sumDigits(333);
		System.out.println(sumDigits(333));
	}

	public static int sumDigits(int number) {
		int x0 = number % 10;
		number = number / 10;
		int x1 = number % 10;
		number = number / 10;
		int x2 = number % 10;

		return x0 + x1 + x2;
	}
}