package ru.inno;

/**
 * 29.10.2020
 * 20. Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Smile {
    public static void main(String[] args) {
        Integer i1 = 129;
        Integer i2 = 129;

        System.out.println(i1 == i2);
    }
}
