package ru.inno;

import java.util.Random;
import java.util.Scanner;

public class MainGetClass {

    public static void main(String[] args) {
	    Object object = new Object();
	    Scanner scanner = new Scanner(System.in);
	    Random random = new Random();
        System.out.println(scanner.getClass().getName());
        System.out.println(random.getClass().getName());

        Runnable runnable = () -> System.out.println("Hello!");

        System.out.println(runnable.getClass().getName());
    }
}
