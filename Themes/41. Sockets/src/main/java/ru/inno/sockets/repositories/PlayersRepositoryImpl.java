package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PlayersRepositoryImpl implements PlayersRepository {
    public Player findByNickname(String nickname) {
        return new Player(nickname);
    }

    @Override
    public void save(Player firstPlayer) {
        
    }
}
