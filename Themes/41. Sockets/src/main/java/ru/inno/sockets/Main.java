package ru.inno.sockets;

import ru.inno.sockets.server.GameServer;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        GameServer gameServer = new GameServer();
        gameServer.start(7777);
    }
}
