package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GamesRepository {
    void save(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
