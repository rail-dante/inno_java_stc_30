package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

/**
 * 23.12.2020
 * 41. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player firstPlayer);
}
