package ru.inno.hashmap;

import ru.inno.Map;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrimitiveHashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {
        // в качестве индекса берем хеш-код числа
        // хеш-код гораздо больше индекса!
        // 42323 -> 1010010101010011(2)
        // 10 -> 1010(2)
        // 42323 & 10 -> 010 -> 2(10)
        // 1010010101010011
        //             1010
        // 0000000000000010
        // 42323 & 15 -> 3(10)
        // 1010010101010011
        //             1111
        // 0000000000000011
        int index = key.hashCode() & (entries.length - 1);
        entries[index] = new MapEntry<>(key, value);
    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1);
        return entries[index].value;
    }
}
