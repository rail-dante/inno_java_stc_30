package ru.inno.primitive;

import ru.inno.Human;
import ru.inno.Map;

// ассоциативный массив - map - таблица - карта - словарь
public class Main {

    public static void main(String[] args) {
        Map<String, Integer> map = new PrimitiveMapImpl<>();

        // int array[] = new int[10];
        // array[6] = 10;
        // array["Марсель"] = "Привет";
        // -> array.put("Марсель", "Привет");
        map.put("Марсель", 26);
        map.put("Денис", 30);
        map.put("Илья", 28);
        map.put("Неля", 18);
        map.put("Катерина", 23);
        map.put("Полина", 18);
        map.put("Регина", 18);
        map.put("Регина", 25);

        System.out.println(map.get("Полина"));
        System.out.println(map.get("Илья"));
        System.out.println(map.get("Денис"));
        System.out.println(map.get("Регина")); // 25

        Map<String, Human> humanMap = new PrimitiveMapImpl<>();
        humanMap.put("Марсель", new Human("Марсель", "Сидиков"));

        Map<Human, Integer> humanIntegerMap = new PrimitiveMapImpl<>();
        Human ilya = new Human("Илья", "Строев");
        Human anotherIlya = ilya;
        humanIntegerMap.put(ilya, 26);
        humanIntegerMap.put(anotherIlya, 25);

        Human someIlya = new Human("Илья", "Строев");

        // почему должен был вывести 25? Потому что я ему передал ключ
        // с теми же значениями, с какими положил
        // меня вообще не интересует ссылка там или нет
        // int <- null
        // NullPointerException
        int ageOfIlya = humanIntegerMap.get(someIlya);
        System.out.println(ageOfIlya);
    }
}
