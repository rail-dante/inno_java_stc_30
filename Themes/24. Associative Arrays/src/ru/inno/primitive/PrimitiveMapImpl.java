package ru.inno.primitive;

import ru.inno.Map;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

    // put, get -> O(N)
    // a[i] = 5
    // int i = a[3];
    // мы понимаем, что индекс в массиве позволяет очень быстро
    // получить информацию, потому что индекс - сдвиг относительно начала массива
    // a[3] = адрес(a[0]) + int_size * 3
    // наша задача -> преобразовать любой ключ в индекс массива (см. hashes)
public class PrimitiveMapImpl<K, V> implements Map<K, V> {
    // как решить проблему фиксированного размера?
    // сделать авторасширение
    private static final int DEFAULT_SIZE = 10;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];
    private int count = 0;

    private static class MapEntry<K, V> {
        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // у меня дублируются ключи
    // нельзя, чтобы дублировались!
    @Override
    public void put(K key, V value) {
        // найти ключ
        MapEntry<K, V> existedEntry = null;
        // ищем совпадающий ключ
        for (int i = 0; i < count; i++) {
            if (entries[i].key.equals(key)) {
                // если нашли, то запомнили entry с этим ключом
                existedEntry = entries[i];
            }
        }

        // если такой ключ уже был
        if (existedEntry != null) {
            // заменяем значение, которое было
            existedEntry.value = value;
        } else {
            // если ключа не было, добавляем новый элемент в массив
            this.entries[count] = new MapEntry<>(key, value);
            count++;
        }
    }

    @Override
    public V get(K key) {
        for (int i = 0; i < count; i++) {
            if (this.entries[i].key.equals(key)) {
                return this.entries[i].value;
            }
        }
        return null;
    }
}
