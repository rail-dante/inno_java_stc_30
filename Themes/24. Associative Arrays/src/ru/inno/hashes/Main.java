package ru.inno.hashes;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// по итогу, нам необходимо завернуть хеш в индекс массива
    // максимальный индекс массива - это максимальное значение int-а
    // мы никогда не получим идеальную хеш-функцию, поскольку
    // целевое множество ограничено, а исходное - нет
public class Main {

    public static int simpleStringHash(String string) {
        char characters[] = string.toCharArray();
        int hash = 0;
        int mult = 1;
        for (int i = 0; i < characters.length; i++) {
            hash += characters[i] * mult;
            mult = mult * 10;
        }
        // hash = sum: s[i] * 10^i
        return hash;
    }
    public static void main(String[] args) {
        // a -> b
        // c -> d
        // d -> b
        // коллизия
        String s1 = "ABC";
        String s2 = "DEF";
        String s3 = "CBA";
        System.out.println(simpleStringHash(s1));
        System.out.println(simpleStringHash(s2));
        System.out.println(simpleStringHash(s3));
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s3.hashCode());
    }
}
