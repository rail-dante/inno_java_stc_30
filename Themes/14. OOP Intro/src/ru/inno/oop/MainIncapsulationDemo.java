package ru.inno.oop;

/**
 * 08.10.2020
 * 14. OOP Intro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainIncapsulationDemo {
    public static void main(String[] args) {
        Human h1 = new Human(1.99, 23);
        Human h2 = new Human(1.99, 10);
        Human h3 = new Human(1.99, 23);
        Human h4 = new Human(1.99, 24);
        Human h5 = new Human(1.99, 25);
        Human h6 = new Human(1.99, 25);
        Human h7 = new Human(1.99, 25);

        int ages[] = new int[100];

        Human humans[] = {h1, h2, h3, h4, h5, h6, h7};

//        h5.age = -10;
        h5.setAge(-10);

        for (int i = 0; i < humans.length; i++) {
//            int currentAge = humans[i].age;
            int currentAge = humans[i].getAge();
            // ex. currentAge = 23
            ages[currentAge]++; // ages[23]++ -> 1, ++ -> 2
        }

        int i = 0;
    }
}
