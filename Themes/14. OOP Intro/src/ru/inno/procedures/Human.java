package ru.inno.procedures;

/**
 * 08.10.2020
 * 14. OOP Intro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    // fields, поля, определяют состояние объекта

    // поля (синонимы) -> свойства, характеристики
    boolean isWorker;
    double height;

    // methods, методы, функции и процедуры внутри класса
    // определяют поведение объекта
    void grow(double value) {
        height += value;
    }

    void relax() {
        isWorker = false;
    }

    void work() {
        isWorker = true;
    }
}
