package ru.inno.relationship;

/**
 * 08.10.2020
 * 14. OOP Intro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Bus {
    private int number;
    private Driver driver;

    private Passenger passengers[];
    private int passengersCount;

    public Bus(int number) {
        // TODO: добавить проверку
        this.number = number;
        this.passengers = new Passenger[10];
    }

    public void takePassenger(Passenger passenger) {
        this.passengers[passengersCount] = passenger;
        this.passengersCount++;
    }

    public void takeDriver(Driver driver) {
        this.driver = driver;
    }

    public void go() {
        System.out.println("Автобус " + this.number + " поехал!");
        for (int i = 0; i < passengersCount; i++) {
            System.out.println(passengers[i].getFirstName() + " я поехал!");
        }
    }
}
