package ru.inno.collections;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// мой Iterable параметризуется буквой B
// при этом эта буква B попадает в Iterator в позицию буквы A
public interface Iterable<B> {
    Iterator<B> iterator();
}
