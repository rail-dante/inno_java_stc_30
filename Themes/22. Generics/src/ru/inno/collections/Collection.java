package ru.inno.collections;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// вы наследуете методы Iterable но со своим типом C
public interface Collection<C> extends Iterable<C> {
    void add(C element);
    boolean contains(C element);
    int size();
    void removeFirst(C element);
}
