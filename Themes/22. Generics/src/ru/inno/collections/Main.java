package ru.inno.collections;

import ru.inno.intro.Nokia3310;
import ru.inno.intro.iPhone;

import java.util.ArrayList;


/**
 * 02.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // iPhone -> Iterable B -> Iterator A
        Iterable<iPhone> iterable = null;
        Iterator<iPhone> iterator = iterable.iterator();

        Collection<String> strings = null;
        Iterator<String> stringIterator = strings.iterator();
        strings.add("Hello");
        strings.removeFirst("Строка");
//        strings.add(11);

        List<iPhone> iPhoneList = null;
        iPhoneList.add(new iPhone());
//        iPhoneList.add(new Nokia3310());
        Iterator<iPhone> iPhoneIterator = iPhoneList.iterator();

        // String -> LinkedList(E) -> List(D) -> Collection(C) -> Iterable(B) -> Iterator(A)
        // String -> LinkedList(String) -> List(String) -> Collection(String) -> Iterable(String) -> Iterator(A)
        List<String> stringList = new LinkedList<>();
        stringList.add("Hello");
//        stringList.add(10);
        String hello = stringList.get(0);
//        int v1 = stringList.get(0);
        Iterator<String> stringIterator1 = stringList.iterator();
        String line = stringIterator1.next();
    }
}
