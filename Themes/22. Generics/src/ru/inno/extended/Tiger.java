package ru.inno.extended;

/**
 * 05.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Tiger extends Animal {
    @Override
    public String toString() {
        return "Тигр";
    }
}
