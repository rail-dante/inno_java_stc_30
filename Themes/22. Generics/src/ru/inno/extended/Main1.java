package ru.inno.extended;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// List - плохо
// List<T> - хорошо
// List<T> не предок List<E>, даже если T предок E
// List<?> - предок всего, но добавлять нельзя
/**
 * 02.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main1 {
    // List<T>
    // List -> raw type

    // данный метод принимает любой список
    public static void printRawList(List list) {
        System.out.println("--> RAW");
        // при этом дает возможность положить в исходный список что угодно
        list.add(new Scanner(System.in));
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }

    }

    // представим что можно передать List<Animal>
    public static void printObjectsList(List<Object> list) {
        System.out.println("--> OBJECTS");
        list.add(new Scanner(System.in));
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
    }

    // List<Animal>
    // Animal <- Wolf <- Dog
    // Animal <- Tiger

    // List<Animal> !!! <- !!! List<Wolf>
    // List<Animal> !!! <- !!! List<Dog>
    // List<Animal> !!! <- !!! List<Tiger>

    // представим, что я передал список тигров
    public static void printAnimalsList(List<Animal> list) {
        System.out.println("--> ANIMALS");
        list.add(new Dog());
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
    }


    // ? - позволяет не использовать rawType, но разрешает любые объекты
    // НО ЗАПРЕЩАЕТ ЧТО-ТО КЛАСТЬ
    public static void printWildcardList(List<?> list) {
//        list.add(new Object());
//        list.add(new Tiger());
        list.remove(0);
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
    }


    public static void main(String[] args) {
        // класть можно что-угодно, получаем тоже что-угодно, никакого контроля
        List rawList = new ArrayList();
        rawList.add(new Animal());
        rawList.add(new Wolf());
        rawList.add(new Dog());
        rawList.add(new Scanner(System.in));

        Object valueFromList = rawList.get(2);

        //---

        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal());
        animals.add(new Wolf());
        animals.add(new Dog());

        List<Wolf> wolves = new ArrayList<>();
        wolves.add(new Wolf());
        wolves.add(new Dog());
//        wolves.add(new Animal());

        List<Dog> dogs = new ArrayList<>();
        dogs.add(new Dog());
//        dogs.add(new Wolf());
//        dogs.add(new Animal());

        List<Tiger> tigers = new ArrayList<>();
        tigers.add(new Tiger());
        tigers.add(new Tiger());
        //---
        printRawList(rawList);
        printRawList(animals);
        printRawList(wolves);
        printRawList(dogs);

        //---
        printObjectsList(rawList);
//        printObjectsList(animals);
//        printObjectsList(wolves);
//        printObjectsList(dogs);

        printAnimalsList(rawList);
        printAnimalsList(animals);
//        printAnimalsList(wolves);
//        printAnimalsList(dogs);
//        printAnimalsList(tigers);

        printWildcardList(rawList);
        printWildcardList(animals);
        printWildcardList(wolves);
        printWildcardList(tigers);
    }

}
