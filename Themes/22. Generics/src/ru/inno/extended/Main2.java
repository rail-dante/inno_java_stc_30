package ru.inno.extended;

import java.util.ArrayList;
import java.util.List;

/**
 * 05.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {

    // List<?> - список неизвестного типа. Если мы получаем объект из этого списка, он имеет тип Object
    // в такой список нельзя добавлять элементы

    // UPPER BOUNDS
    // список, состоящий из объектов-потомков класса Wolf (все!)
    // я могу получить элемент списка, как экземпляр Wolf
    // автоматически порисходит явное восходящее преобразование
    public static void printWolvesChildren(List<? extends Wolf> wolvesChildren) {
        for (int i = 0; i < wolvesChildren.size(); i++) {
            Wolf wolf = wolvesChildren.get(0);
            System.out.println(wolf.toString());
            // я не могу положить ни один из экземпляров классов-потомков и самого класса тоже
            // почему?
            // потому что если передали список собак, я не хочу, чтобы там был волк
            // если передали список хороших мальчиков, мне не нужна там собака или волк
//            wolvesChildren.add(new Wolf());
//            wolvesChildren.add(new Dog());
//            wolvesChildren.add(new GoodBoy());

//            Dog dog = (Dog) wolvesChildren.get(0);
//            GoodBoy goodBoy = (GoodBoy) wolvesChildren.get(0);
        }
    }

    // LOWER BOUNDS
    // список, состоящий из экземпляров классов-предков Dog
    public static void printDogsParents(List<? super Dog> dogsParents) {
        for (int i = 0; i < dogsParents.size(); i++) {
            // мы не можем получать данные, потому что мы не знаем, какой-там тип
            // если получаем собаку - а вдруг там волки? я же не могу преобразовать волка к собаке
            // если получаем волка - а вдруг там животные? я же не могу преобразовать животное к волку
            // если получаем животное - а вдруг там еще что-то сверху?
//            Animal animal = dogsParents.get(0);
//            Wolf wolf = dogsParents.get(0);
//            Dog dog = dogsParents.get(0);
        }
        // почему я могу положить собаку и хорошего мальчика?
        // потому что в этом списке точно ПРЕДКИ этих классов - волки, собаки, животные
        dogsParents.add(new Dog());
        dogsParents.add(new GoodBoy());
//        dogsParents.add(new Wolf());
//        dogsParents.add(new Animal());
    }

    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal());
        animals.add(new Wolf());
        animals.add(new Dog());

        List<Wolf> wolves = new ArrayList<>();
        wolves.add(new Wolf());
        wolves.add(new Dog());

        List<Dog> dogs = new ArrayList<>();
        dogs.add(new Dog());

        List<Tiger> tigers = new ArrayList<>();
        tigers.add(new Tiger());
        tigers.add(new Tiger());

        List<GoodBoy> goodBoys = new ArrayList<>();
        goodBoys.add(new GoodBoy());
        goodBoys.add(new GoodBoy());

        printWolvesChildren(wolves);
        printWolvesChildren(dogs);
        printWolvesChildren(goodBoys);
//        printWolvesChildren(tigers);
//        printWolvesChildren(animals);

        printDogsParents(dogs);
        printDogsParents(wolves);
        printDogsParents(animals);
//        printDogsParents(tigers);
//        printDogsParents(goodBoys);
    }
}
