package ru.inno.extended;

/**
 * 02.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Animal {
    @Override
    public String toString() {
        return "Животное!";
    }
}
