package ru.inno.intro;

/**
 * 02.11.2020
 * 22. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Nokia3310 implements Phone {
    public void ring() {
        System.out.println("Звоним!");
    }
}
