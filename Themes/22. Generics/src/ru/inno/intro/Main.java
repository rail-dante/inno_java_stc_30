package ru.inno.intro;

public class Main {

    public static void main(String[] args) {
	    iPhone iPhone = new iPhone();
	    Nokia3310 nokia3310 = new Nokia3310();
	    Cover<Nokia3310> cover = new Cover<>();
		cover.setPhone(nokia3310);
//		cover.setPhone(iPhone);

		Nokia3310 nokia3310FromCover = cover.getPhone();
		nokia3310FromCover.ring();

		Cover<iPhone> cover1 = new Cover<>();
		cover1.setPhone(iPhone);
		iPhone iPhoneFromCover = cover1.getPhone();

		Cover cover2 = new Cover();
		cover2.setPhone(nokia3310);
		cover2.setPhone(iPhone);
		Object object = cover2.getPhone();

		Cover<Phone> cover3 = new Cover<>();
		cover3.setPhone(iPhone);
		cover3.setPhone(nokia3310);

		Phone phone = cover3.getPhone();
    }
}
